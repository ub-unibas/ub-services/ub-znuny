### installing znuny on a host with db in a container OR external DB
- in "Dockerfile-znuny[-k8s]", set ZNUNY_VERSION to desired version (new versions might cause adaptions to the build process!)
- check volumes and environment-vars in docker-compose.yml
- make sure backup directory is mounted as a volume in znuny-container
- $ docker-compose build
- $ docker-compose up -d
- restore backup (see section below)
- go to http://[ip]/znuny/index.pl

### installing znuny on k8s
- build Dockerfile-znuny-k8s locally
- push it to cr.gitlab.switch.ch/ub-unibas/ub-znuny/znuny:latest
- kubectl apply -f k8s-manifests
- wait till "/opt/otrs" exists (if not, container is still busy doing its copyjob)
- restore backup (see section below)
- go to http://[ingress-url]/znuny/index.pl

### restore backup
- set up docker container running znuny
- make sure the DB to use is empty or non existant
- log into a shell of the container: $ docker exec -it znuny_httpd bash (for k8s, use k8s-tools to do so)
- in the container:
  - in /opt/znuny/Kernel/Config.pm: change values:
    - $ Self->{Home} = '/opt/znuny';
    - $ Self->{'DatabaseHost'} = '[HOSTNAME]';
    - change 'Database', 'DatabaseUser', 'DatabasePw' and 'DatabaseHost' if needed
  - $ cp /opt/znuny/scripts/apache2-httpd.include.conf /opt/znuny/scripts/apache2-httpd.include.conf.original
  - $ cd /opt/znuny/scripts
  - $ ./restore.pl -b /opt/data/backup/ -d /opt/znuny/
  - check/change again values of /opt/znuny/Kernel/Config.pm
  - cp /opt/znuny/scripts/apache2-httpd.include.conf.original /opt/znuny/scripts/apache2-httpd.include.conf
  - start cron+daemon:
    - $ su otrs -c '/opt/znuny/bin/znuny.Daemon.pl start'
    - $ su otrs -c '/opt/znuny/bin/Cron.sh start'
