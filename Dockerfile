FROM ubuntu:latest
MAINTAINER Matthias Edel (matthias.edel@unibas.ch)

ARG ZNUNY_VERSION=6.0.48

# install needed packages
RUN apt update \
	&& apt install -y wget apache2 mariadb-client cpanminus cron libdbd-odbc-perl vim
#	&& apt install -y wget apache2 mariadb-client mariadb-server cpanminus cron

# Download and extact Znuny
RUN cd /opt \
	&& wget https://download.znuny.org/releases/znuny-${ZNUNY_VERSION}.tar.gz \
	&& tar xfz znuny-${ZNUNY_VERSION}.tar.gz

# Add user, copy default config and set permissions
RUN useradd -d /opt/znuny-${ZNUNY_VERSION} -c 'Znuny user' -g www-data -s /bin/bash -M -N otrs \
    && useradd -d /opt/znuny -c 'Znuny user' -g www-data -s /bin/bash -M -N znuny \
	&& cp /opt/znuny-${ZNUNY_VERSION}/Kernel/Config.pm.dist /opt/znuny-${ZNUNY_VERSION}/Kernel/Config.pm \
    && /opt/znuny-${ZNUNY_VERSION}/bin/otrs.SetPermissions.pl --znuny-user=otrs

# Rename default cronjobs
RUN	su - otrs -c "cd /opt/znuny-${ZNUNY_VERSION}/var/cron && for foo in *.dist; do cp \$foo \$(basename \$foo .dist); done"

# copy config.pm here

# install perl modules
RUN apt -y install libapache2-mod-perl2 libdbd-mysql-perl libtimedate-perl libnet-dns-perl libnet-ldap-perl libio-socket-ssl-perl libpdf-api2-perl libsoap-lite-perl libtext-csv-xs-perl libjson-xs-perl libapache-dbi-perl libxml-libxml-perl libxml-libxslt-perl libyaml-perl libarchive-zip-perl libcrypt-eksblowfish-perl libencode-hanextra-perl libmail-imapclient-perl libtemplate-perl libdatetime-perl libmoo-perl bash-completion libyaml-libyaml-perl libjavascript-minifier-xs-perl libcss-minifier-xs-perl libauthen-sasl-perl libauthen-ntlm-perl libical-parser-perl

# configure/prepare apache2
RUN chmod +x /etc/apache2/envvars && /etc/apache2/envvars
RUN ln -s /opt/znuny-${ZNUNY_VERSION}/scripts/apache2-httpd.include.conf /etc/apache2/conf-available/zzz_znuny.conf
RUN echo "RedirectMatch ^/$ /znuny/index.pl" >> /etc/apache2/apache2.conf
RUN a2enmod perl headers deflate filter cgi \
	&& a2dismod mpm_event \
	&& a2enmod mpm_prefork \
	&& a2enconf zzz_znuny

# install nullmailer
RUN DEBIAN_FRONTEND=noninteractive apt install -y nullmailer \
    && echo "smtp.unibas.ch" > /etc/nullmailer/remotes

# change ownership and permissions in filesystem
RUN find /opt/znuny-${ZNUNY_VERSION}/var/httpd -type d -exec chmod 775 {} \; \
	&& find /opt/znuny-${ZNUNY_VERSION}/var/httpd -type f -exec chmod 664 {} \;
RUN chown -R otrs:www-data /tmp

RUN mkdir -p /opt/data/znuny-installation \
    && ln -s /opt/znuny-${ZNUNY_VERSION} /opt/znuny \
    && rm /etc/apache2/conf-available/zzz_znuny.conf \
    && ln -s /opt/znuny/scripts/apache2-httpd.include.conf /etc/apache2/conf-available/zzz_znuny.conf

# setup cronjob checking if Daemon is running
RUN touch /opt/cronjob_start_daemon.log \
    && chown otrs:www-data /opt/cronjob_start_daemon.log \
    && crontab -l | { cat; echo "*/5 * * * * echo \"\$(date +'%Y-%m-%d %H:%M:%S') Starting OTRS Daemon:\" >> /opt/cronjob_start_daemon.log 2>&1"; echo "*/5 * * * * /opt/otrs/bin/znuny.Daemon.pl start >> /opt/cronjob_start_daemon.log 2>&1"; } | crontab -
# correct syntax for crontab entries:
#*/5 * * * *    printf "\n\n[\%s] Starting OTRS Daemon:" "$(date +"\%Y-\%m-\%d \%H:\%M:\%S")" >> /opt/cronjob_start_daemon.log 2>&1
#*/5 * * * *    /opt/otrs/bin/znuny.Daemon.pl start >> /opt/cronjob_start_daemon.log 2>&1
