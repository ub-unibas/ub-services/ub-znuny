#!/bin/bash

otrs_daemon_process_name="perl /opt/znuny/bin/znuny.Daemon.pl start"

# Check if the OTRS daemon process is running
if ps aux | grep -v grep | grep "$otrs_daemon_process_name" > /dev/null; then
    echo -e "[$(date)] znuny daemon is already running."
else
    # Start the OTRS daemon
    echo -e "[$(date)] znuny daemon is not running. Starting..."
    su -c "./znuny/bin/znuny.Daemon.pl start" -s /bin/bash znuny
fi
